import React from "react";
import { Image } from "react-native";
import {
  Container,
  Content,
  Button,
  Thumbnail,
  Card,
  CardItem
} from "native-base";
import { AppHeader, AppFooter } from "../app-nav/index";

var Analytics = require('react-native-firebase-analytics');

export default class PromoMenu extends React.Component {
  componentWillMount() {
    Analytics.setUserId('2');
    Analytics.setUserProperty('promo', 'propertyValue');

    Analytics.logEvent('promo', {
      'item_id': 'promo'
    });
    Analytics.setScreenName('promo_menu')
  }
  render() {
    return (
      <Container>
        <AppHeader isMenu navigation={this.props.navigation} title="Telkom Corner" />
        <Content>
          <Button style={{ height: 100, width: '100%', alignSelf: 'center' }} transparent onPress={() => this.props.navigation.navigate("Promo")}>
            <Card transparent>
              <CardItem cardBody>
                <Image style={{ width: '100%' }} source={require("../../img/asset/promo/group_4.png")} />
              </CardItem>
            </Card>
          </Button>
          <Button style={{ height: 100, width: '100%', alignSelf: 'center' }} transparent onPress={() => this.props.navigation.navigate("PetaGrapari", { titlePeta: "Grapari" })}>
            <Card transparent>
              <CardItem cardBody>
                <Image style={{ width: '100%' }} source={require("../../img/asset/promo/group_3.png")} />
              </CardItem>
            </Card>
          </Button>
          <Button style={{ height: 100, width: '100%', alignSelf: 'center' }} transparent onPress={() => this.props.navigation.navigate("PetaGrapari", { titlePeta: "wifi .id" })}>
            <Card transparent>
              <CardItem cardBody>
                <Image style={{ width: '100%' }} source={require("../../img/asset/promo/wifi_id.png")} />
              </CardItem>
            </Card>
          </Button>
          <Button style={{ height: 100, width: '100%', alignSelf: 'center' }} transparent onPress={() => this.props.navigation.navigate("Zakat")}>
            <Card transparent>
              <CardItem cardBody>
                <Image style={{ width: '100%' }} source={require("../../img/asset/promo/zakat.png")} />
              </CardItem>
            </Card>
          </Button>
        </Content>
      </Container>
    );
  }

  go(param) {
    if (param == "promo") this.props.navigation.navigate("Promo");
  }
};